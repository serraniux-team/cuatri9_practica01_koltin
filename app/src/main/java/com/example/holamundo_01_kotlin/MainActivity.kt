package com.example.holamundo_01_kotlin

import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import android.view.View
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
    fun saludar(v: View){
        val nombreUser = findViewById<EditText>(R.id.txtUser)
        Toast.makeText(this, "Hola ${nombreUser.text}", Toast.LENGTH_LONG).show()
    }
}
